# Alliance Auth BBS
A simple forum for Alliance Auth. Emphasis on simple.

## Installation

`pip install git+https://gitlab.com/allianceauth/allianceauth-bbs.git`

Add to your `INSTALLED_APPS`
```
    'bbs',
    'martor',
```

Collect staticfiles `python manage.py collectstatic`

Run migrations `python manage.py migrate`

Restart your WSGI workers.
